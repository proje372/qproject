from django.conf.urls import include, url
from django.contrib import admin

from main_app.views import *

urlpatterns = [
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^studentLogin/$', studentLogin, name='studentLogin'),
    url(r'^teacherLogin/$', teacherLogin, name='teacherLogin'),
    url(r'^createAccount/$', createAccount, name='createAccount'),
    url(r'^studentWelcome/$', studentWelcome, name='studentWelcome'),
    url(r'^createTest/$', createTest, name='createTest'),
    url(r'^results/$', results, name='results'),
    url(r'^question/$', question, name='question'),
    url(r'^teacherWelcome/$', teacherWelcome, name='teacherWelcome'),
    url(r'^showStatistics/$', showStatistics, name='showStatistics'),
    url(r'^createQuestion/$', createQuestion, name='createQuestion'),
    url(r'^register/$', register, name='register'),
    url(r'^loginCheck/$', loginCheck, name='loginCheck'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^find_question/', findQuestions, name='findQuestions' )
]
