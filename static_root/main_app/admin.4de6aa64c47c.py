from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Teacher)
admin.site.register(Course)
admin.site.register(Chapter)
admin.site.register(Section)
admin.site.register(Subsection)
admin.site.register(Student)
admin.site.register(Question)
admin.site.register(FixedQuestion)
admin.site.register(ParameterizedQuestion)
admin.site.register(Parameter)
admin.site.register(Test)
admin.site.register(TestQuestion)
