from django.db import models

# Create your models here.

class Teacher(models.Model):
    teacher_first_name = models.CharField(max_length=40)
    teacher_last_name = models.CharField(max_length=20)
    teacher_password = models.CharField(max_length=40)

    def __str__(self):
        return self.teacher_first_name + " " + self.teacher_last_name

class Course(models.Model):
    course_id = models.IntegerField(primary_key=True)
    course_section = models.IntegerField()
    course_name = models.CharField(max_length=50)
    given_by = models.ManyToManyField(Teacher)

    class Meta:
        unique_together = (('course_id', 'course_section'),)

    def __str__(self):
        return self.course_name

class Chapter(models.Model):
    chapter_name = models.CharField(max_length=50)
    course_id = models.ForeignKey(Course)

    def __str__(self):
        return str(self.id)+" "+self.chapter_name

class Section(models.Model):
    section_name = models.CharField(max_length=50)
    chapter_id = models.ForeignKey(Chapter)

    def __str__(self):
        return str(self.id)+" "+self.section_name

class Subsection(models.Model):
    subsection_name = models.CharField(max_length=50)
    section_id = models.ForeignKey(Section)
    prerequisite_id = models.CharField(max_length=10)
    recursive_relation = models.ForeignKey('self',null=True,blank=True)

    def __str__(self):
        return str(self.id)+" "+self.subsection_name

class Student(models.Model):
    student_number = models.IntegerField()
    student_username = models.CharField(max_length=40)
    student_first_name = models.CharField(max_length=40)
    student_last_name = models.CharField(max_length=20)
    student_department = models.CharField(max_length=50,default="Math")
    student_year = models.IntegerField()
    student_password = models.CharField(max_length=50)

    takes = models.ManyToManyField(Course)

    def __str__(self):
        return self.student_first_name + " " + self.student_last_name

class Question(models.Model):
    question_id = models.IntegerField()
    subsection = models.ForeignKey(Subsection)
    teacher_id = models.ForeignKey(Teacher)

    class Meta:
        unique_together = ('question_id','subsection')

    def __str__(self):
        return str(self.question_id)

class FixedQuestion(Question):
    fixed_question_text = models.CharField(max_length=1000)
    fixed_question_answer = models.CharField(max_length=5)
    fixed_option_a = models.CharField(max_length=200)
    fixed_option_b = models.CharField(max_length=200)
    fixed_option_c = models.CharField(max_length=200)
    fixed_option_d = models.CharField(max_length=200)

    def __str__(self):
        return "(fixed question id = "+ str(self.question_id) + ") (subsection id = "+ str(self.subsection)+")"

class ParameterizedQuestion(Question):
    param_question_text = models.CharField(max_length=1000)
    param_question_answer = models.CharField(max_length=5)
    param_option_a = models.CharField(max_length=200)
    param_option_b = models.CharField(max_length=200)
    param_option_c = models.CharField(max_length=200)
    param_option_d = models.CharField(max_length=200)

    def __str__(self):
        return "(parameterized question) (question id = "+ str(self.question_id) +")"


class Parameter(models.Model):
    param_question_id = models.ForeignKey(ParameterizedQuestion,on_delete=models.CASCADE)
    parameter = models.CharField(max_length=200)
    class Meta:
       unique_together = ('param_question_id','parameter')

    def __str__(self):
        return "(parameters) ="+ str(self.param_question_id)

class Test(models.Model):
    test_no = models.IntegerField()
    test_date = models.DateField()
    semester = models.CharField(max_length=20)
    success_ratio = models.FloatField()
    student = models.ForeignKey(Student,on_delete=models.CASCADE)
    answers = models.ManyToManyField(Question,through='TestQuestion')

    class Meta:
        unique_together = ('student','test_no')

    def __str__(self):
        return "(test) (student id = "+ str(self.student_id)+ ")"


class TestQuestion(models.Model):
    test = models.ForeignKey(Test)
    question = models.ForeignKey(Question)
    answer = models.CharField(max_length=5)

    def __str__(self):
        return "(question)  (test id = "+str(self.test_id) +") (question id ="+ str(self.question_id)+")"
