from django.core.urlresolvers import reverse
from django.shortcuts import *
import random
import json
import datetime
import xlwt
import xlrd
from io import StringIO
import io
import csv
import sys
import time

from .models import *

def login(request):
    if (request.session.has_key('username')):
        del request.session['username']
    return render(request, "login.html",{})

def loginCheck(request):
    username = request.GET.get("username")
    password = request.GET.get("password")
    if Student.objects.filter(student_username=username, student_password=password).exists():
        request.session['username'] = username
        request.session['type'] = 'student'
        response = HttpResponse(
            json.dumps({'type': 'student'}), content_type='application/json')
        response.status_code = 200
        return response
    elif Teacher.objects.filter(teacher_username=username, teacher_password=password).exists():
            request.session['username'] = username
            request.session['type'] = 'teacher'
            response = HttpResponse(
                json.dumps({'type': 'teacher'}), content_type='application/json')
            response.status_code = 200
            return response
    else:
        response = HttpResponse(
            json.dumps({'message': "Kullanıcı adı veya parola yanlış."}), content_type='application/json')
        response.status_code = 401
        return response

def saveQuestion(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        type = request.GET.get("type")
        subsection = request.GET.get("subsection")
        text = request.GET.get("question_text")
        option_a = request.GET.get("option_a")
        option_b = request.GET.get("option_b")
        option_c = request.GET.get("option_c")
        option_d = request.GET.get("option_d")
        answer = request.GET.get("answer")
        sub = Subsection.objects.get(subsection_name=subsection)
        #id= Question.objects.all().count()+1
        if type == "Parametreli":
            paramQ = ParameterizedQuestion(param_question_text=text, param_question_answer=answer,
                                           param_option_a=option_a, param_option_b=option_b,
                                           param_option_c=option_c, param_option_d=option_d,
                                           subsection=sub, teacher_id=teacher
                                           )
            paramQ.save()
            return HttpResponse(status=200)
        elif type == "Parametresiz":
            fixedQ = FixedQuestion(fixed_question_text=text, fixed_question_answer=answer,
                                   fixed_option_a=option_a, fixed_option_b=option_b, fixed_option_c=option_c,
                                   fixed_option_d=option_d,subsection=sub, teacher_id=teacher)
            fixedQ.save()
            return HttpResponse(status=200)
    else:
        return render(request, "login.html", {})

def createAccount(request):
    courses = Course.objects.all()
    return render(request, "createAccount.html",{'courses':courses})

def studentWelcome(request):
    if(request.session.has_key('username')):
        username=request.session['username']
        student = Student.objects.get(student_username=username)
        return render(request, "student/studentWelcome.html", {'student':student})
    else:
        return render(request,"login.html", {})

def createTest(request):
    if (request.session.has_key('username')):
        subsections = Subsection.objects.all()
        sections = Section.objects.all()
        chapters = Chapter.objects.all()
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        return render(request, "student/createTest.html",
                      {'chapters': chapters, 'sections': sections, 'subsections': subsections, 'student': student})
    else:
        return render(request, "studentLogin.html", {})

def results(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        tests = student.test_set.all()
        testQuestions=TestQuestion.objects.all()
        return render(request, "student/results.html", {'student': student, 'tests':tests, 'testquestions':testQuestions})
    else:
        return render(request, "login.html", {})

class StaticVariables:      
    question_list={}
    param_question_id = {}
    param_test_id = {}
    test_list={}
    true={}
    false={}
    blank={}
    answers={}

    subsections={}
    tests = {}
    testQuestions = {}
    result_student = {}

    randoms = {}
    success_ratio={}
    section = ""
    ManualQuizSubsections={}

    section_fromstatistics = ""
    studentnumber_fromstatistics = ""
    quiz_id=""

    quiz_question_list = {}
    quiz_id_2 = 0

def createTestQuestions(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        question_list = []
        questions_on_test = []
        answers=[]
        question_id = []
        test_id=[]
        true=0; false=0;blank=0;
        my_success_ratio = {}
        list_of_subsections = request.GET.getlist(("list_of_subsections[]"))
        for subsection in list_of_subsections:
            my_success_ratio.update({subsection:[0,0,0]})
            list = FixedQuestion.objects.filter(subsection=subsection)
            paramList = ParameterizedQuestion.objects.filter(subsection=subsection)
            for element in list:
                question_list.append(element)
            for element in paramList:
                fake_fixed_question = FixedQuestion(fixed_question_text=element.param_question_text,id=element.id,
                                                    fixed_question_answer=element.param_question_answer,fixed_option_a=element.param_option_a,
                                                    fixed_option_b=element.param_option_b,fixed_option_c=element.param_option_c,
                                                    fixed_option_d=element.param_option_d,teacher_id=element.teacher_id,subsection=element.subsection)
                question_id.append(element.id)
                question_list.append(fake_fixed_question)
        random.shuffle(question_list)

        StaticVariables.success_ratio.update({username:my_success_ratio})
        StaticVariables.question_list.update({username: question_list})
        StaticVariables.test_list.update({username: questions_on_test})
        StaticVariables.true.update({username:true})
        StaticVariables.false.update({username:false})
        StaticVariables.blank.update({username:blank})
        StaticVariables.answers.update({username:answers})
        StaticVariables.param_question_id.update({username:question_id})
        StaticVariables.param_test_id.update({username:test_id})
        return HttpResponse(status=200)
    else:
        return render(request, "login.html", {})

def question(request):
    if (request.session.has_key('username')):
        next_question = findQuestion(request)
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        if not next_question:
            return saveTestToDatabase(request)
        else:
            return render(request, "student/question.html", {'question': next_question, 'student':student})
    else:
        return render(request, "login.html", {})

def saveTestToDatabase(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        test_id = Test.objects.filter(student__student_username=username).count() + 1
        true=StaticVariables.true[username]
        false=StaticVariables.false[username]
        blank=StaticVariables.blank[username]
        success_ratio = "{0:.2f}".format(true/(true+false+blank)*100)
        true_number=true; false_number=false; blank_number=blank;
        student = Student.objects.get(student_username=username)
        solvedTest = Test(test_no=test_id,test_date=datetime.datetime.today().strftime('%Y-%m-%d'),semester=Course.objects.get(course_id=1),success_ratio=success_ratio,
                          true_number=true,false_number=false,blank_number=blank,student=student)
        solvedTest.save()
        if (len(StaticVariables.test_list[username])!=len(StaticVariables.answers[username])):
            ignored_question = StaticVariables.test_list[username][len(StaticVariables.test_list)-1]
            StaticVariables.test_list[username].remove(ignored_question)

        for i in range(0,len(StaticVariables.test_list[username])):
            q=StaticVariables.test_list[username][i]
            for param_id in StaticVariables.param_test_id[username]:
                if(param_id==StaticVariables.test_list[username][i].id):
                    q=ParameterizedQuestion.objects.get(id=param_id)

            a=StaticVariables.answers[username][i]
            TestAndQuestion = TestQuestion(test=solvedTest,question=q,answer=a)
            TestAndQuestion.save()
        return HttpResponseRedirect(reverse(results))
    else:
        return render(request, "login.html", {})

def saveQuestionToTest(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        choosenOption = request.GET.get("choosenOption")
        rightAnswer = request.GET.get("rightAnswer")
        subsection_id = request.GET.get("subsection_id")

        if(choosenOption==rightAnswer):
            currentValue=StaticVariables.true[username]
            StaticVariables.true.update({username:currentValue+1})
            StaticVariables.answers[username].append("T")
            StaticVariables.success_ratio[username][subsection_id][0] += 1
        elif (choosenOption is None):
            currentValue = StaticVariables.blank[username]
            StaticVariables.blank.update({username: currentValue + 1})
            StaticVariables.answers[username].append("B")
            StaticVariables.success_ratio[username][subsection_id][2] += 1
        elif (choosenOption!=rightAnswer):
            currentValue=StaticVariables.false[username]
            StaticVariables.false.update({username:currentValue+1})
            StaticVariables.answers[username].append("F")
            StaticVariables.success_ratio[username][subsection_id][1] += 1
        else:
            return HttpResponse(status=500)
        true=StaticVariables.success_ratio[username][subsection_id][0]
        false=StaticVariables.success_ratio[username][subsection_id][1]
        blank=StaticVariables.success_ratio[username][subsection_id][2]
        if(true+false+blank>=4):
            false_rate = (false + blank ) / (true + false + blank) * 100
            if(false_rate>=75):
                will_be_removed=[]
                will_be_removed_id=[]
                for q in StaticVariables.question_list[username]:
                    if str(q.subsection_id) == str(subsection_id):
                        will_be_removed.append(q)
                        if q.id in StaticVariables.param_question_id[username]:
                            will_be_removed_id.append(q.id)
                for q_ in will_be_removed:
                    StaticVariables.question_list[username].remove(q_)
                for q_id_ in will_be_removed_id:
                    StaticVariables.param_question_id[username].remove(q_id_)
                subsection = Subsection.objects.get(id=subsection_id)
                if(not subsection.prerequisite_id == ""):
                    previous_subsection = Subsection.objects.get(id=subsection.prerequisite_id)
                    list = FixedQuestion.objects.filter(subsection=previous_subsection)
                    paramList = ParameterizedQuestion.objects.filter(subsection=previous_subsection)
                    my_success_ratio = {}
                    my_success_ratio.update({str(previous_subsection.id): [0, 0, 0]})

                    StaticVariables.success_ratio[username].update(my_success_ratio)

                    for element in list:
                        StaticVariables.question_list[username].append(element)
                    for element in paramList:
                        fake_fixed_question = FixedQuestion(fixed_question_text=element.param_question_text,
                                                            id=element.id,
                                                            fixed_question_answer=element.param_question_answer,
                                                            fixed_option_a=element.param_option_a,
                                                            fixed_option_b=element.param_option_b,
                                                            fixed_option_c=element.param_option_c,
                                                            fixed_option_d=element.param_option_d,
                                                            teacher_id=element.teacher_id,
                                                            subsection=element.subsection)
                        StaticVariables.param_question_id[username].append(element.id)
                        StaticVariables.question_list[username].append(fake_fixed_question)
        return HttpResponse(status=200)
    else:
        return render(request, "login.html", {})

def teacherWelcome(request):
    if (request.session.has_key('username') ):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        return render(request, "teacher/teacherWelcome.html", {'teacher': teacher})
    else:
        return render(request, "login.html", {})

def createStatistics(request):
    section=request.GET.get("Section")
    studentNo=request.GET.get("studentNo")
    if(Student.objects.filter(student_number=studentNo).exists()):
        student = Student.objects.get(student_number=studentNo)
    else:
        student = None
    subsect=[]
    tst = []
    tstQ = []

    if section=="Konuyu Seçiniz":
        subsect=Subsection.objects.all()
        StaticVariables.section_fromstatistics = section

    else:
        section = Section.objects.get(section_name=section)
        subsect=Subsection.objects.filter(section_id=section)
        StaticVariables.section_fromstatistics = section.section_name

    tst = Test.objects.filter(student=student)

    tstQ = TestQuestion.objects.filter(test=tst)
    #tstQ = []
    #for sub in subsect:
    #    tstQ.append(TestQuestion.objects.filter(question__subsection=sub))
    StaticVariables.subsections=subsect
    StaticVariables.tests=tst
    StaticVariables.testQuestions=tstQ
    StaticVariables.result_student = student

    return HttpResponse(status=200)

def teacherResults(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        return render(request, "teacher/teacherResults.html", {'teacher': teacher,
                                                       'subsections':StaticVariables.subsections,
                                                       'tests':StaticVariables.tests,
                                                       'test_questions':StaticVariables.testQuestions,
                                                       'student': StaticVariables.result_student,
                                                       'section': StaticVariables.section_fromstatistics})
    else:
        return render(request, "login.html", {})

def showStatistics(request):
    if (request.session.has_key('username')):
        sections=Section.objects.all()
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        return render(request, "teacher/showStatistics.html", {'teacher': teacher, 'sections': sections})
    else:
        return render(request, "login.html", {})

def createQuestion(request):
    if (request.session.has_key('username') ):
        subsections = Subsection.objects.all()
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        return render(request, "teacher/createQuestion.html", {'teacher': teacher, 'subsections':subsections})
    else:
        return render(request, "login.html", {})

def createRandomQuiz(request):
    subsections = request.GET.getlist("subsections[]")
    numQuestions = request.GET.getlist("numQuestions[]")
    quiz_name = request.GET.get("quiz_name")
    due_date = request.GET.get("due_date")
    question_list=[]
    total_num_questions=0
    for j in range(0,len(numQuestions)):
        total_num_questions = total_num_questions+int(numQuestions[j])

    quiz=Quiz(name=quiz_name, number_of_questions=total_num_questions,creation_date=datetime.datetime.today().strftime('%Y-%m-%d'),
              due_date=due_date,accessibility=False)
    quiz.save()
    for i in range(0,len(subsections)):
        subsection = Subsection.objects.get(id=int(subsections[i]))
        questions = Question.objects.filter(subsection=subsection)
        quiz_subsection = QuizSubsection(quiz=quiz,subsection=subsection)
        quiz_subsection.save()
        for q in questions:
            question_list.append(q)
        for j in range(0,int(numQuestions[i])):
            rand = random.randint(0, len(question_list) - 1)
            print(str(rand))
            question = question_list[rand]
            q_object = Question.objects.get(id=question.id)
            print(q_object)
            quiz_question = QuizQuestion(quiz=quiz, question=q_object)
            question_list.remove(question)
            quiz_question.save()
    return HttpResponse(status=200)

def createManuelQuiz(request):
    subsections = request.GET.getlist(("Subsections[]"))
    Subsections=[]
    for subsection in subsections:
        Subsections.append(Subsection.objects.get(id=subsection))
    StaticVariables.ManualQuizSubsections=Subsections
    return HttpResponse(status=200)

def renderManualQuizQuestions(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        subsections=StaticVariables.ManualQuizSubsections
        fixedquestions=FixedQuestion.objects.all()
        paramquestions=ParameterizedQuestion.objects.all()
        return render(request, "teacher/createManuelQuiz.html", {'teacher': teacher, 'subsections': subsections, 'fixquestions':fixedquestions, 'paramquestions':paramquestions})
    else:
        return render(request, "login.html", {})

def createQuiz(request):
    if (request.session.has_key('username') ):
        fixedQuestions = FixedQuestion.objects.all()
        paramQuestions = ParameterizedQuestion.objects.all()
        subsections = Subsection.objects.all()
        sections=Section.objects.all()
        chapters = Chapter.objects.all()
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        return render(request, "teacher/createQuiz.html", {'teacher': teacher, 'subsections':subsections, 'sections':sections, 'chapters':chapters,
                                                   'fixedQuestions': fixedQuestions,'paramQuestions': paramQuestions})
    else:
        return render(request, "login.html", {})

def register(request):
    username = request.GET.get("username").strip()
    number = request.GET.get("number").strip()
    name = request.GET.get("name").strip()
    surname = request.GET.get("surname").strip()
    password = request.GET.get("password").strip()
    password2 = request.GET.get("password2").strip()
    course = request.GET.get("course").strip()

    if(username == "" or number == "" or name == "" or
               surname=="" or password == "" or password2==""):
        response = HttpResponse(json.dumps({'message': "Lütfen tüm alanları doldurunuz.", 'field': ""}), content_type='application/json')
        response.status_code = 401
        return response

    if(' ' in username):
        response = HttpResponse(json.dumps({'message': "Kullanıcı adı boşluk içeremez.", 'field': "username"}),
                                content_type='application/json')
        response.status_code = 401
        return response

    if(not number.isdigit()):
        response = HttpResponse(json.dumps({'message': "Numara yalnız sayılardan oluşmalıdır. ", 'field': "number"}), content_type='application/json')
        response.status_code = 401
        return response

    if(Student.objects.filter(student_number=number).exists()):
        response = HttpResponse(json.dumps({'message': "Bu numarada bir öğrenci var. ", 'field': "number"}),
                                content_type='application/json')
        response.status_code = 401
        return response

    if (not Student.objects.filter(student_username=username).exists()):
        if(password2 == password):
            st = Student(student_number=number,student_first_name=name, student_last_name=surname,
                         student_password=password, student_username=username,
                         )
            st_course = Course.objects.get(course_name=course)
            st.save()
            st.takes.add(st_course)
            return HttpResponse(status=200)
        else:
            response = HttpResponse(json.dumps({'message': "Parolalar uyuşmuyor.",'field':"password"}),content_type='application/json')
            response.status_code = 401
            return response
    else:
        response = HttpResponse(json.dumps({'message': "Bu kullanıcı adı kullanılıyor." ,'field':"username"}),content_type='application/json')
        response.status_code = 401
        return response

def saveQuestion(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        type = request.GET.get("type")
        subsection = request.GET.get("subsection")
        text = request.GET.get("question_text")
        option_a = request.GET.get("option_a")
        option_b = request.GET.get("option_b")
        option_c = request.GET.get("option_c")
        option_d = request.GET.get("option_d")
        answer = request.GET.get("answer")
        sub = Subsection.objects.get(subsection_name=subsection)
        id = Question.objects.all().count()+1
        if type == "Parametreli":
            paramTimes=request.GET.get("availableUsageNum")
            parameters=request.GET.getlist(("Parameters[]"))
            maxValues=request.GET.getlist(("MaxValues[]"))
            minValues=request.GET.getlist(("MinValues[]"))
            paramQ = ParameterizedQuestion(id=id,param_question_text=text, param_question_answer=answer,
                                           param_option_a=option_a, param_option_b=option_b,
                                           param_option_c=option_c, param_option_d=option_d,
                                           subsection=sub, teacher_id=teacher,param_times=paramTimes
                                           )
            paramQ.save()
            paramQuestion=ParameterizedQuestion.objects.get(id=id)
            for index in range(0,len(parameters)):
                param = Parameter(param_question_id=paramQuestion,parameter=parameters[index],parameter_min=minValues[index],
                                parameter_max=maxValues[index])
                param.save()
            return HttpResponse(status=200)
        elif type == "Parametresiz":
            fixedQ = FixedQuestion(id=id,fixed_question_text=text, fixed_question_answer=answer,
                                   fixed_option_a=option_a, fixed_option_b=option_b, fixed_option_c=option_c,
                                   fixed_option_d=option_d,subsection=sub, teacher_id=teacher)
            fixedQ.save()
            return HttpResponse(status=200)
    else:
        return render(request, "login.html", {})

def logout(request):
    if(request.session.has_key('username')):
        del request.session['username']

    return render(request, "login.html", {})

def saveQuiz(request):
    questions=request.GET.getlist(("questions[]"))
    quizname=request.GET.get("quizName")
    duedate=request.GET.get("dueDate")
    subsections=request.GET.getlist(("subsections[]"))
    quiz=Quiz(name=quizname, number_of_questions=len(questions),creation_date=datetime.datetime.today().strftime('%Y-%m-%d'),due_date=duedate,accessibility=False)
    quiz.save()
    for question in questions:
        quest=Question.objects.get(id=question)
        quizquestion=QuizQuestion(quiz=quiz,question=quest)
        quizquestion.save()
    for subsection in subsections:
        print(subsection)
        subsec=Subsection.objects.get(id=subsection)
        quizsubsection=QuizSubsection(quiz=quiz,subsection=subsec)
        quizsubsection.save()
    return HttpResponse(status=200)

def quizes(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        quizes=Quiz.objects.all()
        return render(request, "teacher/quizes.html", {'teacher':teacher, 'quizes':quizes})
    else:
        return render(request, "login.html", {})

def deleteQuiz(request):
    quiz_id=request.GET.get("quiz_id")
    quiz=Quiz.objects.get(id=quiz_id)
    quiz.delete()
    return HttpResponse(status=200)

def changeAccessibility(request):
    quiz_id = request.GET.get("quiz_id")
    acces=request.GET.get("accessibility")
    if acces== "true":
        accessiblty=True
    elif acces=="false":
        accessiblty=False
    quiz = Quiz.objects.get(id=quiz_id)
    quiz.accessibility=accessiblty
    quiz.save()
    return HttpResponse(status=200)

def editQuiz(request):
    StaticVariables.quiz_id=request.GET.get("quiz_id")
    return HttpResponse(status=200)

def quizDetails(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        quiz_id = StaticVariables.quiz_id;
        quiz = Quiz.objects.get(id=quiz_id)
        quizquestions = QuizQuestion.objects.filter(quiz=quiz)
        fixquestions=FixedQuestion.objects.all()
        paramquestions=ParameterizedQuestion.objects.all()
        return render(request, 'teacher/quizDetails.html', {'teacher': teacher, 'quiz':quiz, 'quizquestions':quizquestions, 'paramquestions':paramquestions, 'fixquestions':fixquestions})
    else:
        return render(request, "login.html", {})

def saveChanges(request):
    quiz_id = request.GET.get("quiz_id")
    quiz = Quiz.objects.get(id=quiz_id)
    quiz_name=request.GET.get("quiz_name")
    due_date=request.GET.get("due_date")
    quiz.name=quiz_name
    quiz.due_date=due_date
    quiz.save()
    return HttpResponse(status=200)

def find(s, ch):
    lst = [i for i, ltr in enumerate(s) if ltr == ch]
    return lst

def fillRandoms(request, parameters, min, max):
    randm = []
    for i in range(len(parameters)):
        rand = random.randint(int(min[i]), int(max[i]))
        randm.append(rand)
    if request.session.has_key('username'):
        username = request.GET.get("username")
        StaticVariables.randoms.update({username: randm})

def deriveParameters(request,text, parameters):
    indices = []
    if request.session.has_key('username'):
        username = request.GET.get("username")
    else:
        print("derive parameters has no username")
    for i in range(len(parameters)):
        indices.append([])

    for i in range(len(parameters)):
        indices[i].append(find(text, parameters[i]))
        index_list = indices[i][0]
        l = 0
        while (index_list):
            l = index_list[0]
            text = text[:l] + str(StaticVariables.randoms[username][i]) + text[l + 1:]
            index_list = find(text, parameters[i])

    return text

def deriveQuestion(request, question_text, params, answers, min_values, max_values):
    fillRandoms(request, params, min_values, max_values)
    txt = deriveParameters(request, question_text, params)
    ans = []
    for a in answers:
        b = deriveParameters(request, a, params)
        ans.append(b)
    return txt, ans

def exportInfo(request):
    StaticVariables.section=request.GET.get('Section')
    if(request.GET.get('student_number')):
        StaticVariables.studentnumber_fromstatistics = request.GET.get('student_number')
    else:
        StaticVariables.studentnumber_fromstatistics = None
    return HttpResponse(status=200)

def exportExcel(request):
    section = StaticVariables.section
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="results.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Results')

    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Öğrenci Numarası','Öğrenci Adı','Öğrenci Soyadı','Test Numarası', 'Konu', 'Doğru Sayısı', 'Yanlış Sayısı','Boş Sayısı','Başarı Oranı','Tarih']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()

    if(StaticVariables.studentnumber_fromstatistics):
        students = Student.objects.filter(student_number=StaticVariables.studentnumber_fromstatistics)
    else:
        students=Student.objects.all()

    if section=="Konuyu Seçiniz":
        subsections = Subsection.objects.all()
        for student in students:
            tests = Test.objects.filter(student=student)
            testCount = 0
            for test in tests:
                testCount=testCount+1
                subsects=[]
                blankC = []
                trueC = []
                falseC = []
                for subsection in subsections:
                    test_questions = TestQuestion.objects.filter(test=test)
                    for question in test_questions:
                        existence=False
                        if question.question.subsection.id==subsection.id:
                            for i in range(len(subsects)):
                                if subsects[i]==question.question.subsection.subsection_name:
                                    existence=True
                                    if question.answer=="T":
                                        trueC[i]+=1
                                    elif question.answer=="F":
                                        falseC[i]+=1
                                    elif question.answer=="B":
                                        blankC[i]+=1
                            if existence==False:
                                subsects.append(question.question.subsection.subsection_name)
                                if question.answer == "T":
                                    trueC.append(1)
                                    falseC.append(0)
                                    blankC.append(0)
                                elif question.answer == "F":
                                    trueC.append(0)
                                    falseC.append(1)
                                    blankC.append(0)
                                elif question.answer == "B":
                                    trueC.append(0)
                                    falseC.append(0)
                                    blankC.append(1)
                for j in range(len(subsects)):
                    sum = trueC[j] + blankC[j] + falseC[j]
                    if (sum == 0):
                        success = 0
                    else:
                        success = (trueC[j] / sum) * 100
                    array = []
                    array.append(str(student.student_number))
                    array.append(str(student.student_first_name))
                    array.append(str(student.student_last_name))
                    array.append(testCount)
                    array.append(subsects[j])
                    array.append(str(trueC[j]))
                    array.append(str(falseC[j]))
                    array.append(str(blankC[j]))
                    array.append(str(success) + "%")
                    array.append(str(test.test_date))
                    row_num += 1
                    for col_num in range(len(array)):
                        ws.write(row_num, col_num, array[col_num], font_style)
    else:
        sectionObj=Section.objects.get(section_name=section)
        subsections = Subsection.objects.filter(section_id=sectionObj)
        for student in students:
            tests = Test.objects.filter(student=student)
            testCount = 0
            for test in tests:
                testCount =testCount+ 1
                subsects=[]
                blankC = []
                trueC = []
                falseC = []
                for subsection in subsections:
                    test_questions = TestQuestion.objects.filter(test=test)
                    for question in test_questions:
                        existence=False
                        if question.question.subsection.id==subsection.id:
                            for i in range(len(subsects)):
                                if subsects[i]==question.question.subsection.subsection_name:
                                    existence=True
                                    if question.answer=="T":
                                        trueC[i]+=1
                                    elif question.answer=="F":
                                        falseC[i]+=1
                                    elif question.answer=="B":
                                        blankC[i]+=1
                            if existence==False:
                                subsects.append(question.question.subsection.subsection_name)
                                if question.answer == "T":
                                    trueC.append(1)
                                    falseC.append(0)
                                    blankC.append(0)
                                elif question.answer == "F":
                                    trueC.append(0)
                                    falseC.append(1)
                                    blankC.append(0)
                                elif question.answer == "B":
                                    trueC.append(0)
                                    falseC.append(0)
                                    blankC.append(1)

                for j in range(len(subsects)):
                    sum = trueC[j] + blankC[j] + falseC[j]
                    if (sum == 0):
                        success = 0
                    else:
                        success = (trueC[j] / sum) * 100
                    array = []
                    array.append(str(student.student_number))
                    array.append(str(student.student_first_name))
                    array.append(str(student.student_last_name))
                    array.append(testCount)
                    array.append(subsects[j])
                    array.append(str(trueC[j]))
                    array.append(str(falseC[j]))
                    array.append(str(blankC[j]))
                    array.append(str(success) + "%")
                    array.append(str(test.test_date))
                    row_num += 1
                    for col_num in range(len(array)):
                        ws.write(row_num, col_num, array[col_num], font_style)

    wb.save(response)
    return response

def showQuestions(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        parameterized = ParameterizedQuestion.objects.all()
        fixed = FixedQuestion.objects.all()
        return render(request, 'teacher/showQuestions.html', {'teacher' : teacher, 'paramques': parameterized, 'fixques': fixed})
    else:
        return render(request, "login.html", {})

def startQuiz(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        all_quizzes = Quiz.objects.filter(accessibility=True)
        available_quizzes=[]
        solved_quizzes = StudentQuizQuestion.objects.filter(student=student)
        if not solved_quizzes:
            for q in all_quizzes:
                available_quizzes.append(q)
        else:
            for q in all_quizzes:
                append = True
                for sq in solved_quizzes:
                    qq = sq.quiz_question
                    if (qq.quiz.id==q.id):
                        append=False
                if append:
                    if not q in available_quizzes:
                        available_quizzes.append(q)
        return render(request, "student/startQuiz.html", {'student':student, 'quizzes':available_quizzes})
    else:
        return render(request, "login.html", {})

def prepareQuiz(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        quiz_id = request.GET.get('quiz_id')
        StaticVariables.quiz_id_2=quiz_id

        questions=[]
        paramQuestions=[]
        fixedQuestions=[]

        quiz = Quiz.objects.get(id=quiz_id)
        quiz_question_objects = QuizQuestion.objects.filter(quiz=quiz)
        all_params = ParameterizedQuestion.objects.all()
        for quiz_question_object in quiz_question_objects:
            flag = False
            q=quiz_question_object.question
            for pq in all_params:
                if pq.id == q.id:
                    flag = True
            if(flag):
                paramQ = ParameterizedQuestion.objects.get(id=q.id)
                answers = []
                answers.append(paramQ.param_option_a)
                answers.append(paramQ.param_option_b)
                answers.append(paramQ.param_option_c)
                answers.append(paramQ.param_option_d)

                params = Parameter.objects.filter(param_question_id=q.id)
                parameters = []
                min_values = []
                max_values = []
                for param in params:
                    parameters.append(param.parameter)
                    min_values.append(param.parameter_min)
                    max_values.append(param.parameter_max)
                txt, ans = deriveQuestion(request, paramQ.param_question_text, parameters, answers, min_values,
                                          max_values)
                fake_fixed_question = FixedQuestion(fixed_question_text=txt,
                                                    id=paramQ.id,
                                                    fixed_question_answer=paramQ.param_question_answer,
                                                    fixed_option_a=ans[0], fixed_option_b=ans[1],
                                                    fixed_option_c=ans[2], fixed_option_d=ans[3],
                                                    teacher_id=paramQ.teacher_id,
                                                    subsection=paramQ.subsection)
                questions.append(fake_fixed_question)
            else:
                questions.append(FixedQuestion.objects.get(id=q.id))
        #for dummy in questions:
            #print(dummy)
            #print(str(dummy.fixed_question_text))

        StaticVariables.quiz_question_list.update({username:questions})
        #print(str(StaticVariables.quiz_question_list[username]))
        #print("ID of first question of this quiz is: "+ str(qq[0].question.id))
        #question = Question.objects.filter(id=qq.question.id)
        return HttpResponse(status=200)


def findQuizQuestion(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        #print (str(len(StaticVariables.quiz_question_list[username])))
        if not len(StaticVariables.quiz_question_list[username]) == 0:
            if not len(StaticVariables.quiz_question_list[username])==1:
                # test will continue, pick the head element of question list and return it with a False value implying test won't finish.
                next_question=StaticVariables.quiz_question_list[username][0]
                StaticVariables.quiz_question_list[username].remove(next_question)
                return next_question,False
            elif len(StaticVariables.quiz_question_list[username])==1:
                # this is last question remaining in the test, return it with a True value implying test will finish.
                next_question=StaticVariables.quiz_question_list[username][0]
                StaticVariables.quiz_question_list[username].remove(next_question)
                return next_question,True
            else:
                # there is no question in test, return none as a question with True value implying test will finish.
                return None,True
        else:
            return None,True
    else:
        return render(request, "login.html", {})

def findQuestion(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        if username in StaticVariables.question_list:
            question_list = StaticVariables.question_list[username]
            if question_list:
                next_question = question_list[0]
                flag = False
                for element in StaticVariables.param_question_id[username]:
                    if next_question.id == element:
                        StaticVariables.param_question_id[username].remove(element)
                        StaticVariables.param_test_id[username].append(element)
                        flag = True
                if(flag):
                    StaticVariables.question_list[username].remove(next_question)
                    paramQ = ParameterizedQuestion.objects.get(id=next_question.id)
                    answers = []
                    answers.append(paramQ.param_option_a)
                    answers.append(paramQ.param_option_b)
                    answers.append(paramQ.param_option_c)
                    answers.append(paramQ.param_option_d)

                    params = Parameter.objects.filter(param_question_id=next_question.id)
                    parameters = []
                    min_values = []
                    max_values = []
                    for param in params:
                        parameters.append(param.parameter)
                        min_values.append(param.parameter_min)
                        max_values.append(param.parameter_max)
                    txt, ans = deriveQuestion(request, next_question.fixed_question_text,parameters, answers, min_values, max_values)

                    fake_fixed_question = FixedQuestion(fixed_question_text=txt,
                                                        id=next_question.id,
                                                        fixed_question_answer=next_question.fixed_question_answer,
                                                        fixed_option_a=ans[0], fixed_option_b=ans[1],
                                                        fixed_option_c=ans[2], fixed_option_d=ans[3],
                                                        teacher_id=next_question.teacher_id,
                                                        subsection=next_question.subsection)

                    next_question=fake_fixed_question
                    StaticVariables.test_list[username].append(fake_fixed_question)
                else:
                    StaticVariables.question_list[username].remove(next_question)
                    StaticVariables.test_list[username].append(next_question)
            else:
                return None
            return next_question
        else:
            return None
    else:
        return render(request, "login.html", {})

def quizQuestion(request):
    if(request.session.has_key('username')):
        quiz_question,finish_test= findQuizQuestion(request)
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        # to set button's text properly
        stquizquestions=StudentQuizQuestion.objects.filter(student=student)
        fquestions=FixedQuestion.objects.all()
        pquestions=ParameterizedQuestion.objects.all()
        next_str="Sonraki Soru";finish_str="Quiz'i Bitir"
        if not finish_test:
            return render(request, "student/quizQuestion.html", {'student':student,'quiz_question':quiz_question,'button':next_str})
        elif quiz_question:
            return render(request, "student/quizQuestion.html", {'student':student,'quiz_question':quiz_question,'button':finish_str})
        else:
            return render(request, "student/studentQuizResults.html", {'student': student,'studentQuizQuestions':stquizquestions,'fquestions':fquestions,'pquestions':pquestions})
    else:
        return render(request,"login.html", {})

def saveQuestionToQuiz(request):
    if(request.session.has_key('username')):
        username = request.session['username']
        q_id = request.GET.get("question_id")                                               # get id of question that student has just solved
        student = Student.objects.get(student_username=username)                            # get who is solving this quiz
        quiz = Quiz.objects.get(id=StaticVariables.quiz_id_2)                               # get which quiz is being solved
        question = Question.objects.get(id=q_id)                                            # get question with id
        quiz_question_object = QuizQuestion.objects.get(quiz=quiz,question=question)        # get QuizQuestion object to save StudentQuizQuestion to database
        student_answer = request.GET.get("choosenOption")                                   # get student's answer to the question
        solve_date = datetime.datetime.today()                                              # get when the quesion is being solved
        #time.strftime("%d/%m/%Y")
        #print(str(student)+"\n")
        #print(str(quiz_question_object)+"\n")
        #print(str(student_answer)+"\n")
        #print(str(solve_date)+"\n")
        if student_answer is None:
            student_answer=""
        student_quiz_question = StudentQuizQuestion(student=student,quiz_question=quiz_question_object,
                                                    solve_date=solve_date,answer=student_answer)
        #print("is problem saving")
        #print(student_quiz_question)
        student_quiz_question.save()
        return HttpResponse(status=200)
    else:
        return render(request,"login.html",{})

def question(request):
    if (request.session.has_key('username')):
        next_question = findQuestion(request)
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        if not next_question:
            return saveTestToDatabase(request)
        else:
            return render(request, "student/question.html", {'question': next_question, 'student':student})
    else:
        return render(request, "login.html", {})

def studentQuizResults(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        student = Student.objects.get(student_username=username)
        stquizquestns=StudentQuizQuestion.objects.filter(student=student)
        fquestions=FixedQuestion.objects.all()
        pquestions=ParameterizedQuestion.objects.all()
        return render(request, "student/studentQuizResults.html", {'student': student,'studentQuizQuestions':stquizquestns,'fquestions':fquestions,'pquestions':pquestions})
    else:
        return render(request, "login.html", {})

def teacherQuizResults(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        quizes=Quiz.objects.all();
        stquizquestions=StudentQuizQuestion.objects.all();
        return render(request, 'teacher/teacherQuizResults.html', {'teacher': teacher,'quizes': quizes,'stquizquestions':stquizquestions})
    else:
        return render(request, "login.html", {})

def deleteQuizQuestion(request):
    quiz_id=request.GET.get('quiz_id')
    question_id=request.GET.get('question_id')
    quiz=Quiz(id=quiz_id)
    question=Question(id=question_id)
    quizquestion=QuizQuestion.objects.get(quiz=quiz,question=question)
    print(quizquestion)
    quizquestion.delete()
    return HttpResponse(status=200)

def addQuestion(request):
    quiz_id = request.GET.get('quiz_id')
    StaticVariables.quiz_id=quiz_id
    return HttpResponse(status=200)

def addQuizQuestion(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        quiz_id=StaticVariables.quiz_id
        quiz=Quiz(id=quiz_id)
        quizQuestions=QuizQuestion.objects.filter(quiz=quiz)
        qsubsections=QuizSubsection.objects.filter(quiz=quiz)
        fquestions=[]
        Fquestions=[]
        pquestions=[]
        Pquestions=[]
        fixedquestions = FixedQuestion.objects.all()
        paramquestions = ParameterizedQuestion.objects.all()
        for fixedquestion in fixedquestions:
            fquestions.append(fixedquestion.id)
        for paramquestion in paramquestions:
            pquestions.append(paramquestion.id)
        for index in range(len(fquestions)):
            for quizQuestion in quizQuestions:
                if fquestions[index] == quizQuestion.question.id:
                    fquestions[index]= -1
        for index in range(len(pquestions)):
            for quizQuestion in quizQuestions:
                if pquestions[index] == quizQuestion.question.id:
                    pquestions[index]= -1
        for fquestion in fquestions:
            if fquestion != -1:
                Fquestions.append(fquestion)
        fixedQuestions=FixedQuestion.objects.filter(id__in=Fquestions)
        for pquestion in pquestions:
            if pquestion != -1:
                Pquestions.append(pquestion)
        paramQuestions = FixedQuestion.objects.filter(id__in=Pquestions)
        return render(request, 'teacher/addQuizQuestion.html', {'teacher': teacher,'quiz_id':quiz_id,'fquestions':fixedQuestions,'pquestions':paramQuestions,'qsubsections':qsubsections})
    else:
        return render(request, "login.html", {})

def saveNewQuizQuestion(request):
    quiz_id = request.GET.get('quiz_id')
    questions=request.GET.getlist(("questions[]"))
    quiz=Quiz(id=quiz_id)
    for question in questions:
        questionn=Question(id=question)
        quizQues=QuizQuestion(question=questionn,quiz=quiz)
        quizQues.save()
    return HttpResponse(status=200)

def exportQuizResults(request):
    quiz=request.GET.get('Quiz')
    StaticVariables.quiz=quiz
    return HttpResponse(status=200)

def exportExcelQuiz(request):
    quiz=StaticVariables.quiz
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="quiz_results.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('QuizResults')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['Öğrenci Numarası', 'Öğrenci Adı', 'Öğrenci Soyadı', 'Quiz Adı','Konu', 'Doğru Sayısı',
               'Yanlış Sayısı', 'Boş Sayısı', 'Başarı Oranı','Tarih']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()
    students=Student.objects.all()
    if quiz == "Quizi Seçiniz":
        for student in students:
            stquizquestions = StudentQuizQuestion.objects.filter(student=student)
            quizes=[]
            dates=[]
            subsections=[]
            trues=[]
            falses=[]
            blanks=[]
            for stquizquestion in stquizquestions:
                existence=False
                for i in range(len(quizes)):
                    if quizes[i] == stquizquestion.quiz_question.quiz:
                        if subsections[i] == stquizquestion.quiz_question.question.subsection:
                            existence=True
                            fquestions=FixedQuestion.objects.all()
                            for fquestion in fquestions:
                                if fquestion.id == stquizquestion.quiz_question.question.id:
                                    if fquestion.fixed_question_answer==stquizquestion.answer:
                                        trues[i]=trues[i]+1
                                    elif stquizquestion.answer=="":
                                        blanks[i]=blanks[i]+1
                                    else:
                                        falses[i]=falses[i]+1
                            pquestions = ParameterizedQuestion.objects.all()
                            for pquestion in pquestions:
                                if pquestion.id == stquizquestion.quiz_question.question.id:
                                    if pquestion.param_question_answer == stquizquestion.answer:
                                        trues[i] = trues[i] + 1
                                    elif stquizquestion.answer == "":
                                        blanks[i] = blanks[i] + 1
                                    else:
                                        falses[i] = falses[i] + 1
                if existence==False:
                    quizes.append(stquizquestion.quiz_question.quiz)
                    subsections.append(stquizquestion.quiz_question.question.subsection)
                    dates.append(stquizquestion.solve_date)
                    fquestions = FixedQuestion.objects.all()
                    for fquestion in fquestions:
                        if fquestion.id == stquizquestion.quiz_question.question.id:
                            if fquestion.fixed_question_answer == stquizquestion.answer:
                                trues.append(1)
                                falses.append(0)
                                blanks.append(0)
                            elif stquizquestion.answer == "":
                                trues.append(0)
                                falses.append(0)
                                blanks.append(1)
                            else:
                                trues.append(0)
                                falses.append(1)
                                blanks.append(0)
                    pquestions = ParameterizedQuestion.objects.all()
                    for pquestion in pquestions:
                        if pquestion.id == stquizquestion.quiz_question.question.id:
                            if pquestion.param_question_answer == stquizquestion.answer:
                                trues.append(1)
                                falses.append(0)
                                blanks.append(0)
                            elif stquizquestion.answer == "":
                                trues.append(0)
                                falses.append(0)
                                blanks.append(1)
                            else:
                                trues.append(0)
                                falses.append(1)
                                blanks.append(0)

            for j in range(len(subsections)):
                sum = trues[j] + blanks[j] + falses[j]
                if (sum == 0):
                    success = 0
                else:
                    success = (trues[j] / sum) * 100
                array = []
                array.append(str(student.student_number))
                array.append(str(student.student_first_name))
                array.append(str(student.student_last_name))
                array.append(quizes[j].name)
                array.append(subsections[j].subsection_name)
                array.append(str(trues[j]))
                array.append(str(falses[j]))
                array.append(str(blanks[j]))
                array.append(str(success) + "%")
                array.append(str(dates[j]))
                row_num += 1
                for col_num in range(len(array)):
                    ws.write(row_num, col_num, array[col_num], font_style)
    else:
        quiz = Quiz.objects.get(name=quiz)
        for student in students:
            stquizquestions = StudentQuizQuestion.objects.filter(student=student)
            quizes = []
            subsections = []
            dates = []
            trues = []
            falses = []
            blanks = []
            for stquizquestion in stquizquestions:
                if(stquizquestion.quiz_question.quiz==quiz):
                    existence = False
                    for i in range(len(quizes)):
                        if quizes[i] == stquizquestion.quiz_question.quiz:
                            if subsections[i] == stquizquestion.quiz_question.question.subsection:
                                existence = True
                                fquestions = FixedQuestion.objects.all()
                                for fquestion in fquestions:
                                    if fquestion.id == stquizquestion.quiz_question.question.id:
                                        if fquestion.fixed_question_answer == stquizquestion.answer:
                                            trues[i] = trues[i] + 1
                                        elif stquizquestion.answer == "":
                                            blanks[i] = blanks[i] + 1
                                        else:
                                            falses[i] = falses[i] + 1
                                pquestions = ParameterizedQuestion.objects.all()
                                for pquestion in pquestions:
                                    if pquestion.id == stquizquestion.quiz_question.question.id:
                                        if pquestion.param_question_answer == stquizquestion.answer:
                                            trues[i] = trues[i] + 1
                                        elif stquizquestion.answer == "":
                                            blanks[i] = blanks[i] + 1
                                        else:
                                            falses[i] = falses[i] + 1
                    if existence == False:
                        quizes.append(stquizquestion.quiz_question.quiz)
                        subsections.append(stquizquestion.quiz_question.question.subsection)
                        dates.append(stquizquestion.solve_date)
                        fquestions = FixedQuestion.objects.all()
                        for fquestion in fquestions:
                            if fquestion.id == stquizquestion.quiz_question.question.id:
                                if fquestion.fixed_question_answer == stquizquestion.answer:
                                    trues.append(1)
                                    falses.append(0)
                                    blanks.append(0)
                                elif stquizquestion.answer == "":
                                    trues.append(0)
                                    falses.append(0)
                                    blanks.append(1)
                                else:
                                    trues.append(0)
                                    falses.append(1)
                                    blanks.append(0)
                        pquestions = ParameterizedQuestion.objects.all()
                        for pquestion in pquestions:
                            if pquestion.id == stquizquestion.quiz_question.question.id:
                                if pquestion.param_question_answer == stquizquestion.answer:
                                    trues.append(1)
                                    falses.append(0)
                                    blanks.append(0)
                                elif stquizquestion.answer == "":
                                    trues.append(0)
                                    falses.append(0)
                                    blanks.append(1)
                                else:
                                    trues.append(0)
                                    falses.append(1)
                                    blanks.append(0)

            for j in range(len(subsections)):
                sum = trues[j] + blanks[j] + falses[j]
                if (sum == 0):
                    success = 0
                else:
                    success = (trues[j] / sum) * 100
                array = []
                array.append(str(student.student_number))
                array.append(str(student.student_first_name))
                array.append(str(student.student_last_name))
                array.append(quizes[j].name)
                array.append(subsections[j].subsection_name)
                array.append(str(trues[j]))
                array.append(str(falses[j]))
                array.append(str(blanks[j]))
                array.append(str(success) + "%")
                array.append(str(dates[j]))
                row_num += 1
                for col_num in range(len(array)):
                    ws.write(row_num, col_num, array[col_num], font_style)
    wb.save(response)
    return response
def createQuizStatistics(request):
    quiz=request.GET.get('Quiz')
    stno=request.GET.get('studentNo')
    StaticVariables.quiz=quiz
    StaticVariables.stno=stno
    return HttpResponse(status=200)

def QuizResults(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        quiz=StaticVariables.quiz
        stno=StaticVariables.stno
        student=Student.objects.get(student_number=stno)
        fquestions=FixedQuestion.objects.all()
        pquestions=ParameterizedQuestion.objects.all()
        if quiz=="Quizi Seçiniz":
            stquizquestions=StudentQuizQuestion.objects.filter(student=student)
        else:
            quizz = Quiz.objects.get(name=quiz)
            quizquestn = QuizQuestion.objects.filter(quiz=quizz)
            stquizquestions=StudentQuizQuestion.objects.filter(student=student).filter(quiz_question=quizquestn)
        StaticVariables.stquizquestions=stquizquestions
        StaticVariables.student=student
        return render(request, "teacher/QuizResults.html", {'teacher': teacher,'student':student,'stquizquestions':stquizquestions,'fquestions':fquestions,'pquestions':pquestions})
    else:
        return render(request, "login.html", {})

def exportStudentQuiz(request):
    return HttpResponse(status=200)

def exportStudentQuizExcel(request):
    student = StaticVariables.student
    stquizquestions = StaticVariables.stquizquestions
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="quiz_results.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('QuizResults')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['Öğrenci Numarası', 'Öğrenci Adı', 'Öğrenci Soyadı', 'Quiz Adı', 'Konu', 'Doğru Sayısı',
               'Yanlış Sayısı', 'Boş Sayısı', 'Başarı Oranı','Tarih']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()
    quizes = []
    subsections = []
    dates=[]
    trues = []
    falses = []
    blanks = []
    fquestions=FixedQuestion.objects.all()
    pquestions=ParameterizedQuestion.objects.all()
    for stquizquestion in stquizquestions:
        existence=False
        for i in range(len(quizes)):
            if quizes[i]==stquizquestion.quiz_question.quiz:
                if subsections[i]==stquizquestion.quiz_question.question.subsection:
                    existence = True
                    for fquestion in fquestions:
                        if fquestion.id==stquizquestion.quiz_question.question.id:
                            if stquizquestion.answer==fquestion.fixed_question_answer:
                                trues[i]=trues[i]+1
                            elif stquizquestion.answer== "":
                                blanks[i]=blanks[i]+1
                            else:
                                falses[i]=falses[i]+1
                    for pquestion in pquestions:
                        if pquestion.id==stquizquestion.quiz_question.question.id:
                            if stquizquestion.answer==pquestion.param_question_answer:
                                trues[i]=trues[i]+1
                            elif stquizquestion.answer== "":
                                blanks[i]=blanks[i]+1
                            else:
                                falses[i]=falses[i]+1
        if existence==False:
            quizes.append(stquizquestion.quiz_question.quiz)
            subsections.append(stquizquestion.quiz_question.question.subsection)
            dates.append(stquizquestion.solve_date)
            for fquestion in fquestions:
                if fquestion.id == stquizquestion.quiz_question.question.id:
                    if stquizquestion.answer == fquestion.fixed_question_answer:
                        trues.append(1)
                        falses.append(0)
                        blanks.append(0)
                    elif stquizquestion.answer == "":
                        trues.append(0)
                        falses.append(0)
                        blanks.append(1)
                    else:
                        trues.append(0)
                        falses.append(1)
                        blanks.append(0)
            for pquestion in pquestions:
                if pquestion.id == stquizquestion.quiz_question.question.id:
                    if stquizquestion.answer == pquestion.param_question_answer:
                        trues.append(1)
                        falses.append(0)
                        blanks.append(0)
                    elif stquizquestion.answer == "":
                        trues.append(0)
                        falses.append(0)
                        blanks.append(1)
                    else:
                        trues.append(0)
                        falses.append(1)
                        blanks.append(0)
    print(stquizquestions)
    for j in range(len(quizes)):
        sum = trues[j] + blanks[j] + falses[j]
        if (sum == 0):
            success = 0
        else:
            success = (trues[j] / sum) * 100
        array = []
        array.append(student.student_number)
        array.append(student.student_first_name)
        array.append(student.student_last_name)
        array.append(quizes[j].name)
        array.append(subsections[j].subsection_name)
        array.append(str(trues[j]))
        array.append(str(falses[j]))
        array.append(str(blanks[j]))
        array.append(str(success) + "%")
        array.append(str(dates[j]))
        row_num += 1
        for col_num in range(len(array)):
            ws.write(row_num, col_num, array[col_num], font_style)
    wb.save(response)
    return response

def importStudent(request):
    if (request.session.has_key('username')):
        username = request.session['username']
        teacher = Teacher.objects.get(teacher_username=username)
        students = Student.objects.all()
        courses = Course.objects.all()
        return render(request, 'teacher/importStudent.html', {'teacher': teacher, 'courses':courses,'students':students})
    else:
        return render(request, "login.html", {})

def importExcel(request):
    print(request.POST)
    if request.method == 'POST' and request.FILES['file']:
        handle_uploaded_file(request.FILES['file'])
    username = request.session['username']
    teacher = Teacher.objects.get(teacher_username=username)
    return render(request, "teacher/teacherWelcome.html", {'teacher': teacher})

def handle_uploaded_file(xls_file):
    file_name = '%s.csv'%(xls_file.name)
    workbook = xlrd.open_workbook(file_contents=xls_file.read())
    all_worksheets = workbook.sheet_names()
    worksheet_name = all_worksheets[0]
    worksheet = workbook.sheet_by_name(worksheet_name)

    csv_output = io.StringIO()
    csv_data = []
    for rownum in range(0,worksheet.nrows):
        csv_data.append([str(entry) for entry in worksheet.row_values(rownum)])
    writer = csv.writer(csv_output)
    for row in csv_data:
        writer.writerow(csv_data)

    print(csv_data)
    #numara, ad soyad, e-mail
    for rownum in range(0,len(csv_data)):
        print(csv_data[rownum])

        st = Student()
        name=[]
        for colnum in range(0,len(csv_data[rownum])):
            if(colnum==0):#numara
                num = csv_data[rownum][colnum]
                num = num[:-2]
                st.student_number=num

            elif(colnum==1):#ad soyad
                txt = csv_data[rownum][colnum]
                space = txt.rfind(' ')
                while(space > 0):
                    data = txt[space+1:len(txt)]
                    txt=txt[:space]
                    sys.stdout.flush()
                    name.insert(0,data)
                    space = txt.rfind(' ')
                name.insert(0,txt)
                if(len(name) == 2):
                    first = name[0]
                    last = name[1]
                else:
                    last = name[len(name)-1]
                    c=0
                    first = ""
                    while(c<len(name)-1):
                        first += name[c] + " "
                        c = c+1

                st.student_last_name = last.strip()
                st.student_first_name = first.strip()
            elif(colnum==2):#email
                email = csv_data[rownum][colnum]
                index = email.find("@")
                username = email[:index]
                st.student_username = username.strip()

        st.student_password = "tobb1234"
        #course = Course.objects.get()
        #st.takes.add(course)
        if (Student.objects.filter(student_number=st.student_number).exists()):
            print("exists")

        else:
            st.save()
    '''response = HttpResponse(json.dumps({'message': "Bu numarada bir öğrenci var. "}),content_type='application/json')
    #response.status_code = 401
    #return response'''
    return HttpResponse(status=200)
