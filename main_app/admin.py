from django.contrib import admin

from .models import *

admin.site.register(Teacher)
admin.site.register(Course)
admin.site.register(Chapter)
admin.site.register(Section)
admin.site.register(Subsection)
admin.site.register(Student)
admin.site.register(FixedQuestion)
admin.site.register(ParameterizedQuestion)
admin.site.register(Parameter)
admin.site.register(Test)
admin.site.register(TestQuestion)
admin.site.register(Quiz)
admin.site.register(StudentQuizQuestion)
admin.site.register(QuizQuestion)
admin.site.register(QuizSubsection)

