# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chapter',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('chapter_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('course_id', models.IntegerField(serialize=False, primary_key=True)),
                ('course_section', models.IntegerField()),
                ('course_name', models.CharField(max_length=50)),
                ('semester', models.CharField(null=True, max_length=50, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Parameter',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('parameter', models.CharField(max_length=200)),
                ('parameter_min', models.IntegerField(default=1)),
                ('parameter_max', models.IntegerField(default=100)),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Quiz',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('number_of_questions', models.IntegerField(default=0)),
                ('creation_date', models.DateField()),
                ('due_date', models.DateField()),
                ('accessibility', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='QuizQuestion',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('section_name', models.CharField(max_length=50)),
                ('chapter_id', models.ForeignKey(to='main_app.Chapter')),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('student_number', models.IntegerField()),
                ('student_username', models.CharField(max_length=40)),
                ('student_first_name', models.CharField(max_length=40)),
                ('student_last_name', models.CharField(max_length=20)),
                ('student_password', models.CharField(max_length=50)),
                ('takes', models.ManyToManyField(to='main_app.Course')),
            ],
        ),
        migrations.CreateModel(
            name='StudentQuizQuestion',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('solve_date', models.DateField()),
                ('answer', models.CharField(null=True, max_length=5, blank=True)),
                ('quiz_question', models.ForeignKey(to='main_app.QuizQuestion')),
                ('student', models.ForeignKey(to='main_app.Student')),
            ],
        ),
        migrations.CreateModel(
            name='Subsection',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('subsection_name', models.CharField(max_length=50)),
                ('prerequisite_id', models.CharField(null=True, max_length=50, blank=True)),
                ('recursive_relation', models.ForeignKey(null=True, to='main_app.Subsection', blank=True)),
                ('section_id', models.ForeignKey(to='main_app.Section')),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('teacher_username', models.CharField(max_length=40)),
                ('teacher_first_name', models.CharField(max_length=40)),
                ('teacher_last_name', models.CharField(max_length=20)),
                ('teacher_password', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('test_no', models.IntegerField()),
                ('test_date', models.DateField()),
                ('success_ratio', models.FloatField(default=0)),
                ('true_number', models.IntegerField(default=0)),
                ('false_number', models.IntegerField(default=0)),
                ('blank_number', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='TestQuestion',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('answer', models.CharField(null=True, max_length=5, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='FixedQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(serialize=False, to='main_app.Question', primary_key=True, parent_link=True, auto_created=True)),
                ('fixed_question_text', models.CharField(max_length=1000)),
                ('fixed_question_answer', models.CharField(max_length=5)),
                ('fixed_option_a', models.CharField(max_length=200)),
                ('fixed_option_b', models.CharField(max_length=200)),
                ('fixed_option_c', models.CharField(max_length=200)),
                ('fixed_option_d', models.CharField(max_length=200)),
            ],
            bases=('main_app.question',),
        ),
        migrations.CreateModel(
            name='ParameterizedQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(serialize=False, to='main_app.Question', primary_key=True, parent_link=True, auto_created=True)),
                ('param_question_text', models.CharField(max_length=1000)),
                ('param_question_answer', models.CharField(max_length=5)),
                ('param_option_a', models.CharField(max_length=200)),
                ('param_option_b', models.CharField(max_length=200)),
                ('param_option_c', models.CharField(max_length=200)),
                ('param_option_d', models.CharField(max_length=200)),
                ('param_times', models.IntegerField(default=1)),
            ],
            bases=('main_app.question',),
        ),
        migrations.AddField(
            model_name='testquestion',
            name='question',
            field=models.ForeignKey(to='main_app.Question'),
        ),
        migrations.AddField(
            model_name='testquestion',
            name='test',
            field=models.ForeignKey(to='main_app.Test'),
        ),
        migrations.AddField(
            model_name='test',
            name='answers',
            field=models.ManyToManyField(through='main_app.TestQuestion', to='main_app.Question'),
        ),
        migrations.AddField(
            model_name='test',
            name='semester',
            field=models.ForeignKey(to='main_app.Course'),
        ),
        migrations.AddField(
            model_name='test',
            name='student',
            field=models.ForeignKey(to='main_app.Student'),
        ),
        migrations.AddField(
            model_name='quizquestion',
            name='question',
            field=models.ForeignKey(to='main_app.Question'),
        ),
        migrations.AddField(
            model_name='quizquestion',
            name='quiz',
            field=models.ForeignKey(to='main_app.Quiz'),
        ),
        migrations.AddField(
            model_name='quiz',
            name='question',
            field=models.ManyToManyField(through='main_app.QuizQuestion', to='main_app.Question'),
        ),
        migrations.AddField(
            model_name='question',
            name='subsection',
            field=models.ForeignKey(to='main_app.Subsection'),
        ),
        migrations.AddField(
            model_name='question',
            name='teacher_id',
            field=models.ForeignKey(to='main_app.Teacher'),
        ),
        migrations.AddField(
            model_name='course',
            name='given_by',
            field=models.ManyToManyField(to='main_app.Teacher'),
        ),
        migrations.AddField(
            model_name='chapter',
            name='course_id',
            field=models.ForeignKey(to='main_app.Course'),
        ),
        migrations.AlterUniqueTogether(
            name='test',
            unique_together=set([('student', 'test_no')]),
        ),
        migrations.AddField(
            model_name='parameter',
            name='param_question_id',
            field=models.ForeignKey(to='main_app.ParameterizedQuestion'),
        ),
        migrations.AlterUniqueTogether(
            name='course',
            unique_together=set([('course_id', 'course_section')]),
        ),
        migrations.AlterUniqueTogether(
            name='parameter',
            unique_together=set([('param_question_id', 'parameter')]),
        ),
    ]
