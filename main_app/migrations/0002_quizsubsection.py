# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuizSubsection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('quiz', models.ForeignKey(to='main_app.Quiz')),
                ('subsection', models.ForeignKey(to='main_app.Subsection')),
            ],
        ),
    ]
